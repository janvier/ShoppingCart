package net.mv.shoppingcart.main;

import java.util.List;

import net.mv.shoppingcart.items.Items;
import net.mv.shoppingcart.items.ItemsDao;

public class ItemsListing {
	
	private ItemsDao itservice= new ItemsDao();
	
	public List<Items> findItems(){
		return itservice.retriverItems();
		 
	}
	
	public List<Items> findSession(){
		return itservice.retriverSession();
		 
	}
	
}
