package net.mv.shoppingcart.main;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.mv.shoppingcart.items.Items;
import net.mv.shoppingcart.items.ItemsDao;
import net.mv.shoppingcart.items.OrderService;
import net.mv.shoppingcart.items.Orders;

public class OrdersCart extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public OrdersCart() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// RETRIEVE ITEMS 
		
		ItemsListing fcs = new ItemsListing();		 
		HttpSession session = request.getSession();
		double total=0;
		List<Items> DelItems = fcs.findSession();
 		if(DelItems == null ){	
 			
 		System.out.println("No content ");
 		} 
		else{
			
			for(Items d: DelItems){
				
			 total+= d.getPrice();	
				
			}
			
		}

        session.setAttribute("TotalCart", DelItems.size());
        session.setAttribute("Total", total);
		session.setAttribute("DelItems", DelItems);
		request.getRequestDispatcher("orders.jsp").forward(request, response);	
	
	       
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Check out orders
		
		HttpSession session = request.getSession();
		Orders ListOr = new Orders();	
		Date time = new Date();
		String Dtm = time.toString();
		OrderService Ors = new OrderService();
		
		long userid = (long) session.getAttribute("UserID");
		
		double total =  (double) session.getAttribute("Total");
		
		int qty = (int) session.getAttribute("TotalCart");
		
		ArrayList<Items> listCart = (ArrayList<Items>) session.getAttribute("CartList");
		
		for( Items OrdersList : listCart){
			
			Ors.addCart(OrdersList.getIt_id(), userid,Dtm, qty, total);
			
			ListOr.setF_id(userid);
			ListOr.setIt_id(OrdersList.getIt_id());
			ListOr.setTime(Dtm);
			ListOr.setQty(qty);
			ListOr.setTotal(total);
		
		}
		
		
		ArrayList<Orders> OrdList = (ArrayList<Orders>) session.getAttribute("OrdersList");
 		
 		if(OrdList == null ){
 			OrdList = new ArrayList<Orders>();		
 		} 		
 		

   
        		//System.out.println("removed");
        		
        		// add the result object to our CartList if or not 
        		OrdList.add(ListOr);
        		// set the result in the session scope	
      	
		// remove all items 
        ItemsDao rmit = new ItemsDao();
        rmit.removeUserIt(userid);		
		System.out.println(" Saved Successfuly");	
		session.setAttribute("OrdList", OrdList);
		doGet(request,response);  	 
		request.getRequestDispatcher("orders.jsp").forward(request, response);
		
			
	}

}
