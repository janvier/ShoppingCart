package net.mv.shoppingcart.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import net.mv.shoppingcart.items.Items;
import net.mv.shoppingcart.items.ItemsDao;
import net.mv.shoppingcart.items.OrderService;

/**
 * Servlet implementation class CartItems
 */
public class CartItems extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CartItems() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
       // ADD ITEMS 
		
		HttpSession session = request.getSession();
       
       Items listtems= new Items();
       
       String inp = request.getParameter("Cart");
       
       String action = request.getParameter("remove");
       
      OrderService Ors = new OrderService();
	
	  long userid = (long) session.getAttribute("UserID");
       
       if(action.equals("true"))
       {
    	   doPost(request,response);  
    	   
       } else{
  
       long it_id = Long.parseLong(inp);
       
       double total =0;
       
      @SuppressWarnings("unchecked")
	ArrayList<Items>itemlist= (ArrayList<Items>) session.getAttribute("ItemsList");
    		   
         for(Items c: itemlist){
        	 
        	if(it_id == c.getIt_id()){
        		
        		Ors.AddIte(userid,c.getIt_id(), c.getName(), c.getVendor(), c.getPrice());
        		
        		listtems.setIt_id(c.getIt_id());
        		listtems.setName(c.getName());
        		listtems.setVendor(c.getVendor());
        		listtems.setPrice(c.getPrice()); 
        		total+= c.getPrice();
        		break;
        		
        	} 
         }
        		@SuppressWarnings("unchecked")
        		ArrayList<Items> CartList = (ArrayList<Items>) session.getAttribute("CartList");
        		
        		if(CartList == null ){
        			CartList = new ArrayList<Items>();		
        		} 		
        		
        		// add the result object to our CartList if or not 
        		CartList.add(listtems);
        		// set the result in the session scope	
        		session.setAttribute("CartList", CartList);
                session.setAttribute("TotalCart", CartList.size());
                session.setAttribute("Total", total);
        		request.getRequestDispatcher("items.jsp").forward(request, response);
       }	
         
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	       
	
		// REMOVE ITEMS 
		
		HttpSession session = request.getSession();
		
		ItemsDao Ors = new ItemsDao();
		
		String inp = request.getParameter("Cart");
		
		String inp2 = request.getParameter("Se");
		
		long it_id = Long.parseLong(inp);
		
		long se_id = Long.parseLong(inp2);
		
		double total=0;
		
		Ors.removeItems(se_id,it_id);
		
		ItemsListing fcs = new ItemsListing();		 
		List<Items> DelItems = fcs.findSession();
		if(DelItems == null ){	
 			
	 		System.out.println("No content ");
	 		
	 		} 
			else{
				
				for(Items d: DelItems){
					
				 total+= d.getPrice();	
					
				}
				
			}

		   session.setAttribute("DelItems", DelItems);

	        session.setAttribute("TotalCart", DelItems.size());
	        session.setAttribute("Total", total);
	
		   request.getRequestDispatcher("orders.jsp").forward(request, response);	
		
		// removed items 
			
	}  

}
