package net.mv.shoppingcart.main;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.mv.shoppingcart.items.Items;
import net.mv.shoppingcart.user.domain.FUser;
import net.mv.shoppingcart.user.service.UserService;

public class ShoppingMain extends HttpServlet {
	
	@SuppressWarnings("null")
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException
	{
		String name= request.getParameter("myName");
		String pass = request.getParameter("myPass");
		UserService reg = new UserService();
		String logout = request.getParameter("log");
		
		System.out.println(logout);
		
		if(logout != null || logout.equals("true")){
			HttpSession session = request.getSession();
			   session.invalidate();	   
			   request.getRequestDispatcher("login.jsp").forward(request, response);
				
			}

		if(name.isEmpty() && pass.isEmpty()){
			
			PrintWriter pw = response.getWriter();
			pw.write("<html>"
					+ "<head>"
					+ "<title> Your need an account to be able to experience our store</title>"
					+ "</head>"
					+ "<body>"
					+ "Your need an account to be a able to add question"
					+ "<p>"
					+ " Uknown registration id"
					+ "</p>"
					+ "</body>"
					+ "</html>");
		   
			}else{	
				
				   if(reg.registerUser(name, pass)!= true){    	   
					   String message = " Your account has been created successfuly :) Please login !!";
					   request.setAttribute("Success", message);
					   request.getRequestDispatcher("register.jsp").forward(request, response);
					   }else{
						   String message = " The account Already exist :( ";
						   request.setAttribute("eXist", message);
						   request.getRequestDispatcher("register.jsp").forward(request, response);
					   }			
				
			}
		
		/*HttpSession session = request.getSession();
		//FlashcardService fcs= new FlashcardService();
		
		String logout = request.getParameter("log");
		
		long userid = (long) session.getAttribute("UserID");
		
		String question = request.getParameter("Question");
		
		String answer = request.getParameter("Answer");
		
		PrintWriter pw = response.getWriter();
		
		if(logout != null && logout.equals("true")){
			   
		   session.invalidate();	   
		   response.sendRedirect("login.jsp");
			
		} else if( question != null && answer != null)
		{	
		    fcs.addFlashCard(question, answer, userid);
		    
            List<FlashCard> cardList = fcs.findFlashCards(userid);
			
			session.setAttribute("cardList", cardList);
			
		    response.sendRedirect("welcome.jsp");		
			
		} else if(logout.equals("false")) {
			
		
		
		pw.write("<html>"
				+"<head> <title> Hello World</title>"+"<link rel='stylesheet' type='text/css' href='/FlashCard/css/style.cs'/>"+
				"</head>" +
				" <body>" +
				" <h1>Add Question! </h1>" +
				"<section class='main'>"+
				" <form action='/FlashCard/home.do' method='get' class='form-1'>"+
				"<p class='field'>"+
				" Question : <input type='text' name='Question'><br>"+
				"</p>" +
				"<p class='field'>"+
                " Answer : <input type='text' name='Answer'><br>"+  
                "</p>" +
                " <input type='submit' name='submit'>"+
			     "</p>" +
			         "</form>" +
		           "</section>" +
				"</body>" + 
				"</html>"	
				);
		
		}else{
			pw.write("<html>"
					+ "<head>"
					+ "<title> Your need an account to be able to add question</title>"
					+ "</head>"
					+ "<body>"
					+ "Your need an account to be a able to add question"
					+ "<p>"
					+ " <a href ='/FlashCard/login.jsp'>Login</a>"
					+ "</p>"
					+ "</body>"
					+ "</html>");
		
			
		}
		*/
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
					
		String name= request.getParameter("myName");
		String pass = request.getParameter("myPass");
		ItemsListing fcs = new ItemsListing();
		
		UserService us= new UserService();
		FUser user = us.authenticateUser(name, pass);
		
		request.setAttribute("myName", name);
					 
		HttpSession session = request.getSession();
				   
		if (user != null && user.isAuthenticated()) {
			   		
			List<Items> cardList = fcs.findItems();
			
			session.setAttribute("ItemsList", cardList);
		    session.setAttribute("UserID", user.getId());
			request.getRequestDispatcher("items.jsp").forward(request, response);	  
			   
		   }else{
			   
			   String message = "Wrong message Please try again !!";
			
			   request.setAttribute("errorM", message);
			   
			   request.getRequestDispatcher("login.jsp").forward(request, response);
			   
		   }
			
		
	}
	
}
