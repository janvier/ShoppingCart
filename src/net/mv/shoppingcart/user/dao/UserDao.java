package net.mv.shoppingcart.user.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import net.mv.shoppingcart.user.domain.FUser;
import net.mv.shoppingcart.user.exception.UserExistsException;
import net.mv.shoppingcart.util.ConnUtil;

public class UserDao {
	
	private ConnUtil util = ConnUtil.getUtil();
	
	public void InsertUser(FUser user) throws UserExistsException{
		
		
	String query = "INSERT INTO F_USER (USERNAME,PASSWORD) VALUES(?,?)";
	
	try (Connection conn= util.createconnection();
			PreparedStatement psmt = conn.prepareStatement(query);){
		
		psmt.setString(1, user.getUsername());
		psmt.setString(2, user.getPassword());
		
		psmt.executeUpdate();
		
	} catch (SQLException e) {	
		
		throw new UserExistsException("The user already exists", e);
		
		// add custom exception
	}		
		
	} // end of insert user
		
    public FUser retrieveUser (String username){
    	
    	String Query ="SELECT * FROM F_USER WHERE USERNAME = ?";
    	
    	FUser fUser= null;
    	
    	try (Connection conn= util.createconnection();
    			PreparedStatement psmt = conn.prepareStatement(Query);){
    		
    		
    		psmt.setString(1, username);
    		
    		ResultSet rs = psmt.executeQuery();
    		
    		if (rs.next()) {
    			
    			fUser = new FUser();
    			
    			fUser.setId(rs.getLong("F_ID"));    			
    			fUser.setUsername(rs.getString("USERNAME"));    			
    			fUser.setPassword(rs.getString("PASSWORD"));
				
			}
    		   		
    					
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	
		return fUser;
    	  	
    }
}
