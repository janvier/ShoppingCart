package net.mv.shoppingcart.user.service;

import net.mv.shoppingcart.user.domain.FUser;
import net.mv.shoppingcart.user.exception.UserExistsException;

public class UserService {

	private net.mv.shoppingcart.user.dao.UserDao UserDao = new net.mv.shoppingcart.user.dao.UserDao();
	
	public boolean registerUser( String username, String password){
		
		FUser user = new FUser();
		
		user.setUsername(username);
		user.setPassword(password);
		
		boolean exists = false;
		
		try {
			
			UserDao.InsertUser(user);
			
		} catch (UserExistsException e) {
			e.printStackTrace();
			
			exists  = true;
		}
				
		return exists;
					
	}

	public FUser authenticateUser(String username, String password){
		FUser user = UserDao.retrieveUser(username);
		
		if (user != null && user.getPassword().equals(password)) {
			user.setAuthenticated(true);
			
		} else {
			
			user = new FUser();
			user.setAuthenticated(false);

		}	
		return user;
			
		
	}
	
	
		
	
}
