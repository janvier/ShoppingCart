package net.mv.shoppingcart.items;

import java.io.Serializable;

public class Orders implements Serializable{
	
	private long it_id;
	private long f_id;
	private String time;
	private int qty;
	private double total;
	@Override
	public String toString() {
		return "Orders [it_id=" + it_id + ", f_id=" + f_id + ", time=" + time + ", qty=" + qty + ", total=" + total
				+ "]";
	}
	public long getIt_id() {
		return it_id;
	}
	public void setIt_id(long it_id) {
		this.it_id = it_id;
	}
	public long getF_id() {
		return f_id;
	}
	public void setF_id(long f_id) {
		this.f_id = f_id;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	

	
	
}
