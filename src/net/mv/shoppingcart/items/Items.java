package net.mv.shoppingcart.items;

import java.io.Serializable;

public class Items implements Serializable {
	
	private long it_id;
	private String name;
	private String vendor;
	private double price;
	private long se_id;

	public long getSe_id() {
		return se_id;
	}
	public void setSe_id(long se_id) {
		this.se_id = se_id;
	}
	
	@Override
	public String toString() {
		return "Items [it_id=" + it_id + ", name=" + name + ", vendor=" + vendor + ", price=" + price + ", se_id="
				+ se_id + "]";
	}
	public long getIt_id() {
		return it_id;
	}
	public void setIt_id(long it_id) {
		this.it_id = it_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

}
