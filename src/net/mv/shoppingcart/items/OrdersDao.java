package net.mv.shoppingcart.items;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.mv.shoppingcart.util.ConnUtil;

public class OrdersDao {
	
private ConnUtil util = ConnUtil.getUtil();
	

public void insertOrders(Orders shopp, long userid){
		
		
		String query ="INSERT INTO ORDERS (IT_ID,F_ID,TIME,QTY,TOTAL) VALUES (?,?,?,?,?)";
			
		try (Connection conn = util.createconnection();
			 PreparedStatement pst= conn.prepareStatement(query);){
		
			 pst.setLong(1,shopp.getIt_id());
			 pst.setLong(2,shopp.getF_id());
			 pst.setString(3,shopp.getTime());
			 pst.setInt(4,shopp.getQty()); 
			 pst.setDouble(5,shopp.getTotal());
			
			pst.executeUpdate();
			 
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
	}
	
	public List<Orders> retrieveOrder( long userid, long it_id){
		
		List<Orders> OrdersList = new ArrayList<Orders>();
		String query = "SELECT * FROM ORDERS WHERE IT_ID =? AND F_ID=?";
		
		try (Connection conn= util.createconnection();
			PreparedStatement pst= conn.prepareStatement(query)){
			
			pst.setLong(1,it_id);
			pst.setLong(2, userid);
			
			pst.executeQuery();
			
			ResultSet rs= pst.getResultSet();
			
			while(rs.next()){	
				
				Orders temp = new Orders();
				temp.setIt_id(rs.getLong("IT_ID"));
				temp.setF_id(rs.getLong("F_ID"));
				temp.setTime(rs.getString("TIME"));
				temp.setQty(rs.getInt("QTY"));
				temp.setTotal(rs.getDouble("TOTAL"));
				OrdersList.add(temp);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		return OrdersList;		
			
	}
	
	
}
