package net.mv.shoppingcart.items;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.mv.shoppingcart.util.ConnUtil;
import net.mv.shoppingcart.items.Items;

public class ItemsDao {
	private ConnUtil util = ConnUtil.getUtil();
	
public List<Items> retriverItems(){
		
		List<Items> cardList = new ArrayList<Items>();
		
		String query = "SELECT * FROM ITEMS";
		
		try (Connection conn= util.createconnection();
    			PreparedStatement psmt = conn.prepareStatement(query);){
			
			ResultSet rs = psmt.executeQuery();
			
			while(rs.next()){
				
				Items card = new Items();
				
				card.setIt_id(rs.getLong("IT_ID"));
				card.setName(rs.getString("NAME"));
				card.setVendor(rs.getString("VENDOR"));
				card.setPrice(rs.getDouble("PRICE"));
				cardList.add(card);
				
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return cardList;
		
	} // end of ITEMS  


public void addItems(Items sess, long userid){
		
		
		String query ="INSERT INTO ITSESSION (F_ID,IT_ID,NAME,VENDOR,PRICE) VALUES (?,?,?,?,?)";
			
		try (Connection conn = util.createconnection();
			 PreparedStatement pst= conn.prepareStatement(query);){
		
			
			 pst.setLong(1,userid);
			 pst.setLong(2,sess.getIt_id());
			 pst.setString(3,sess.getName());
			 pst.setString(4,sess.getVendor()); 
			 pst.setDouble(5,sess.getPrice());
			
			pst.executeUpdate();
			 
			System.out.println("saved!!");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
	}
	
public void removeItems(long se_id,long it_id){
	
	
	String query ="DELETE FROM ITSESSION WHERE SE_ID=? AND IT_ID =?";
		
	try (Connection conn = util.createconnection();
		 PreparedStatement pst= conn.prepareStatement(query);){
		
		 pst.setLong(1,se_id);
		 pst.setLong(2,it_id);		 
		 pst.executeUpdate();
		 
		 System.out.println("deleted items");
		 
	} catch (SQLException e) {
		e.printStackTrace();
	}

}
public void removeUserIt(long userid){
	
	
	String query ="DELETE FROM ITSESSION WHERE F_ID=?";
		
	try (Connection conn = util.createconnection();
		 PreparedStatement pst= conn.prepareStatement(query);){
		
		 pst.setLong(1,userid);	 
		 pst.executeUpdate();
		 
		 System.out.println("deleted all items");
		 
	} catch (SQLException e) {
		e.printStackTrace();
	}

}

public List<Items> retriverSession(){
	
	List<Items> cardList = new ArrayList<Items>();
	
	String query = "SELECT * FROM ITSESSION";
	
	try (Connection conn= util.createconnection();
			PreparedStatement psmt = conn.prepareStatement(query);){
		
		ResultSet rs = psmt.executeQuery();
		
		while(rs.next()){
			
			Items card = new Items();
			
			card.setSe_id(rs.getLong("SE_ID"));
			card.setIt_id(rs.getLong("IT_ID"));
			card.setName(rs.getString("NAME"));
			card.setVendor(rs.getString("VENDOR"));
			card.setPrice(rs.getDouble("PRICE"));
			cardList.add(card);
			
		}
		
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	return cardList;
	
} // end of ITEMS  	

}
