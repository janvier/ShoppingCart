package net.mv.shoppingcart.items;

public class OrderService {
	
	 OrdersDao Osv= new OrdersDao();
	 ItemsDao Itv = new ItemsDao();
	
	public void addCart( long it_id, long userid,String time,int qty,double total){
		
		Orders Shopp = new Orders();
		
		Shopp.setIt_id(it_id);
		Shopp.setF_id(userid);
		Shopp.setTime(time);
		Shopp.setQty(qty);
		Shopp.setTotal(total);
		
		Osv.insertOrders(Shopp, userid);
	}

  public void AddIte( long userid,long it_id,String name,String vendor, double price ){
	  
	  Items sess = new Items();
	  
	  sess.setIt_id(it_id);
	  sess.setName(name);
	  sess.setVendor(vendor);
	  sess.setPrice(price);  
	  
	  Itv.addItems(sess, userid);
	  
  }
  
}
