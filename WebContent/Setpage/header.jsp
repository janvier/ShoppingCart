<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id="fixedheader">
<a href="${pageContext.request.contextPath}/items.jsp">
<img src="<c:url value="/img/logo.png"/>"/> 
</a>
<div style="text-align:right;">
<c:if test="${UserID != null}">[<a href="/ShoppingCart/items.jsp">Items</a> ]</c:if>
<c:if test="${UserID != null}">[<a href="/ShoppingCart/login?log=true">logout</a> ]</c:if>
<c:if test="${UserID == null}"><a href="/ShoppingCart/login.jsp">Login</a></c:if>
</div>
</div>
