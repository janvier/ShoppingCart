<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>
<head>
<title>${param.title}</title>
<link rel="stylesheet" type="text/css"
	href="/ShoppingCart/css/style.css" />
</head>
<body>
<jsp:include page="/Setpage/header.jsp"/>

	<h1>${param.title}</h1>

	<jsp:include page="/pages/${param.content}.jsp"/>
	
	<jsp:include page="/Setpage/footer.jsp"/>
	
</body>
</html>