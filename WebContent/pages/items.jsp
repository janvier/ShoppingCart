
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ page import="net.mv.shoppingcart.items.Items" %>
 <%@ page import="java.util.*" %>
<br>
	
<%-- 		<c:forEach var="items" items="${CartList}" varStatus="status">
              <c:out value="${items.getIt_id()}"></c:out>   
	</c:forEach> --%>
	<br>

<div style="text-align:right;"> <a href="/ShoppingCart/OrdersCart">Cart: [<c:out value="${TotalCart}"></c:out>] Total: $<c:out value="${Total}"></c:out> </a></div>
<form action="/ShoppingCart/CartItems" method="get" >
<table class="listing">
	<tr>
		<th>Id</th>
		<th>Name</th>
		<th>Vendor</th>
		<th>Price</th>
	</tr>
		<c:forEach var="items" items="${ItemsList}" varStatus="status">
		    <tr class="${status.index%2==0 ? 'alt' : ''}">
			<td><a href ="/ShoppingCart/img/<c:out value="${items.getIt_id()}"></c:out>.jpg"><img src="/ShoppingCart/img/<c:out value="${items.getIt_id()}"></c:out>.jpg" width="100px"> </a></td>
			<td><c:out value="${ items.getName() }"></c:out></td>
			<td><c:out value="${ items.getVendor()}"></c:out></td>
			<td>$<c:out value="${ items.getPrice()}"></c:out></td>
			<td width="100px"><a class="myButton" href="/ShoppingCart/CartItems?Cart=${items.getIt_id()}&remove=false">AddToCart</a></td>
		</tr>
	</c:forEach>
</table>
</form>

