
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ page import="net.mv.shoppingcart.items.Items" %>
<%@ page import="net.mv.shoppingcart.items.Orders" %>
<%@ page import="java.util.*" %>
 

<br>
<br>
 <h1> Shopping Cart</h1>

<c:if test="${DelItems != null}">
<div style="text-align:right;"> <a href="#">Cart: [<c:out value="${TotalCart}"></c:out>] Total: $<c:out value="${Total}"></c:out> </a></div>
<form action="/ShoppingCart/OrdersCart" method="post" >
<table class="listing">
	<tr>
		<th>Id</th>
		<th>Name</th>
		<th>Vendor</th>
		<th>Price</th>
		<th><input type="submit" name="submit" value="Checkout" class="myButton" ></th>
	</tr>
		<c:forEach var="items" items="${DelItems}" varStatus="status">
		    <c:if test="${Total != 0.0 && items.getPrice() != 0.0}">
		    <tr class="${status.index%2==0 ? 'alt' : ''}">
			<td><a href ="/ShoppingCart/img/<c:out value="${items.getIt_id()}"></c:out>.jpg"><img src="/ShoppingCart/img/<c:out value="${items.getIt_id()}"></c:out>.jpg" width="100px"> </a></td>
			<td><c:out value="${ items.getName() }"></c:out></td>
			<td><c:out value="${ items.getVendor()}"></c:out></td>
			<td>$<c:out value="${ items.getPrice()}"></c:out></td>
			<%-- <td><input type="hidden" value="${items.getIt_id()}" name="cart" id="cart" /></td> --%>
		    <td width="100px"><a class="myButton" href="/ShoppingCart/CartItems?Cart=${items.getIt_id()}&Se=${items.getSe_id()}&remove=true">Remove</a></td>
		</tr>
		</c:if>
	</c:forEach>
</table>
</form>
</c:if>
<c:if test="${Total == 0.0}"> Your cart is empty!!! </c:if>
<c:if test="${OrdList != null && Total ==  0.0}">
	<br>

	<%
       ArrayList<Orders> calcList = (ArrayList<Orders>) session.getAttribute("OrdList");
 	
		out.print("<br>");
		
		if(calcList != null){
 	
			out.print("<table width='100%' height='30%' style='background-color:white;font-size:20px;'>");
			

			for(Orders c : calcList){
			out.print("<tr>");
			out.print("<td>");	
			out.print("Item ID: " + c.getIt_id() +" : ");
			out.print("</td>");
			out.print("<td>");
			out.print("UserID : " + c.getF_id() +" : ");
			out.print("</td>");
			out.print("<td>");
			out.print("Date : " + c.getTime() +" : ");
			out.print("</td>");
			out.print("<td>");
			out.print(" Items : " + c.getQty());
			out.print("</td>");
			out.print("<td>");
			out.print("Total : $" + c.getTotal());
			out.print("</td>");
			out.print("</tr>");
			}
			
			out.print("</table>");
			
				
		}else{
 	
			out.print("Sory, NO calculator object were in the session scope!!!");
 	
		}	
 	
	%>
	<br><br><br>
<p style="color:white;font-size:40px;"> Thank you !! </p>		

</c:if>
